package com.ppgcc.bancodedados.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.ppgcc.bancodedados.domain.Pedido;

public class PedidoDAO {

	private Connection conn;

	public PedidoDAO(Connection conn) {
		super();
		this.conn = conn;
	}
	
	private Pedido convertIntoObject(ResultSet set) throws SQLException {
		if (set!=null) {
			Long id = set.getLong(1);
			Long idCliente = set.getLong(2);
			LocalDateTime dataHora = set.getTimestamp(3).toLocalDateTime();
			BigDecimal valorTotal = set.getBigDecimal(4);
			return new Pedido (id, idCliente, dataHora, valorTotal);
		}
		return null;
	}
	
	public void deletarPedido(Long id) throws SQLException {
		if(id!=null) {
			PreparedStatement pedidoDeleteStmt;
			pedidoDeleteStmt = conn.prepareStatement(" DELETE FROM Pedido WHERE id = ?; ");
			pedidoDeleteStmt.setLong(1, id);
			pedidoDeleteStmt.execute();
		}
	}
	
	public void deletarTodos() throws SQLException {
		PreparedStatement pedidoDeleteStmt;
		pedidoDeleteStmt = conn.prepareStatement(" DELETE FROM Pedido; ");
		pedidoDeleteStmt.execute();
	}

	public List<Pedido> getPedidos() {
		List<Pedido> pedidos = new ArrayList<>();
		PreparedStatement pedidoStmt;
		try {
			pedidoStmt = conn.prepareStatement(" SELECT id, id_cliente, data_hora, valor_total FROM Pedido ORDER BY id DESC; ");
			ResultSet pedidoResults = pedidoStmt.executeQuery();
			while (pedidoResults.next()) {
				pedidos.add(convertIntoObject(pedidoResults));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return pedidos;
	}
	
	public void inserirPedidoAleatorio(Long idCliente) throws SQLException {
		PreparedStatement pedidoStmt;
		pedidoStmt = conn.prepareStatement(" INSERT INTO Pedido (id_cliente, data_hora, valor_total) VALUES (?, ?, ?); ");
		pedidoStmt.setLong(1, idCliente);
		pedidoStmt.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
		pedidoStmt.setBigDecimal(3, getRandomValorTotal());
		pedidoStmt.execute();
	}
	
	private BigDecimal getRandomValorTotal() {
		String price = "";
		price += Math.round(Math.random()*999);
		price += ".";
		price += Math.round(Math.random()*99);
		return new BigDecimal(price);
	}
	
}