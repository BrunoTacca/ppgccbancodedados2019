package com.ppgcc.bancodedados.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ppgcc.bancodedados.domain.Cliente;

public class ClienteDAO {

	private Connection conn;

	public ClienteDAO(Connection conn) {
		super();
		this.conn = conn;
	}
	
	private Cliente convertIntoObject(ResultSet set) throws SQLException {
		if (set!=null) {
			Long id = set.getLong(1);
			String nome = set.getString(2);
			String cpf = set.getString(3);
			String telefone = set.getString(4);
			return new Cliente (id, nome, cpf, telefone);
		}
		return null;
	}
	
	public void deletarCliente(Long id) throws SQLException {
		if(id!=null) {
			PreparedStatement clienteDeleteStmt;
			clienteDeleteStmt = conn.prepareStatement(" DELETE FROM Cliente WHERE id = ?; ");
			clienteDeleteStmt.setLong(1, id);
			clienteDeleteStmt.execute();
		}
	}
	
	public void deletarTodos() throws SQLException {
		PreparedStatement clienteDeleteStmt;
		clienteDeleteStmt = conn.prepareStatement(" DELETE FROM Cliente; ");
		clienteDeleteStmt.execute();
	}

	public List<Cliente> getClientes() {
		List<Cliente> clientes = new ArrayList<>();
		PreparedStatement clienteStmt;
		try {
			clienteStmt = conn.prepareStatement(" SELECT id, nome, cpf, telefone FROM Cliente ORDER BY id DESC; ");
			ResultSet clienteResults = clienteStmt.executeQuery();
			while (clienteResults.next()) {
				clientes.add(convertIntoObject(clienteResults));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return clientes;
	}
	
	public void inserirClienteAleatorio() throws SQLException {
		PreparedStatement clienteStmt;
		clienteStmt = conn.prepareStatement(" INSERT INTO Cliente (nome, cpf, telefone) VALUES (?, ?, ?); ");
//		for(int i=0; i<100; i++) System.out.println("Cliente: "+new Cliente((long)0, getRandomName(), getRandomCPF(), getRandomTelefone()));
		clienteStmt.setString(1, getRandomName());
		clienteStmt.setString(2, getRandomCPF());
		clienteStmt.setString(3, getRandomTelefone());
		clienteStmt.execute();
	}
	
	private String getRandomName() {
		List<String> fName = Arrays.asList("Bruno", "Evalyn", "Ariane", "Earlean", "Ayanna", "Angel", "Velda", "Salvatore", "Karine", "Marita", "Jerrod", "Phung", "Tresa", "Valerie", "Kamilah", "Long", "Joanne", "Jarrett", "Jammie", "Lilla", "Beulah", "Dorla", "Avis", "Edris", "Connie", "Hallie", "Ilda", "Veronika", "Shenita", "Lisette", "Claretha", "Alonso", "Garland", "Jestine", "Susanne", "Cassi", "Marcelle", "Maya", "Reena", "Marylee", "Valda", "Jan", "Rochel", "Melba", "Usha", "Ling", "Sarai", "Geri", "Michaela", "Eloise");
		List<String> lName = Arrays.asList("Tacca", "Murray", "Campbell", "Petty", "Maldonado", "Kemp", "Ochoa", "Acevedo", "Davidson", "Combs", "Shaw", "Dyer", "Stanley", "Watts", "Parsons", "Schultz", "Buck", "Coleman", "Herring", "Vargas", "Reynolds", "Griffith", "Reyes", "Stephenson", "Higgins", "Haney", "Phillips", "Martinez", "Jensen", "Foster", "Oneill", "Villanueva", "Carrillo", "Khan", "Miller", "Becker", "Howe", "Miranda", "Ayala", "Irwin", "Wilcox", "Stafford", "Miles", "Compton", "Barr", "Vang", "Wagner", "Dudley", "Werner", "Austin", "Calderon");
		int frandom = (int)Math.round(Math.random()*(fName.size()-1));
		int lrandom = (int)Math.round(Math.random()*(lName.size()-1));
		return fName.get(frandom)+" "+lName.get(lrandom);
	}
	
	private String getRandomCPF() {
		String cpf = "";
		cpf += Math.round(Math.random()*(999-100)+100);
		cpf += Math.round(Math.random()*(999-100)+100);
		cpf += Math.round(Math.random()*(999-100)+100);
		cpf += Math.round(Math.random()*(99-10)+10);
		return cpf;
	}
	
	private String getRandomTelefone() {
		String phone = "(";
		phone += Math.round(Math.random()*(99-10)+10);
		phone += ") 9";
		phone += Math.round(Math.random()*(9999-1000)+1000);
		phone += "-";
		phone += Math.round(Math.random()*(9999-1000)+1000);
		return phone;
	}
	
}