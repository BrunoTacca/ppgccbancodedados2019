package com.ppgcc.bancodedados.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ppgcc.bancodedados.domain.Item;

public class ItemDAO {

	private Connection conn;

	public ItemDAO(Connection conn) {
		super();
		this.conn = conn;
	}
	
	private Item convertIntoObject(ResultSet set) throws SQLException {
		if (set!=null) {
			Long id = set.getLong(1);
			Long idProduto = set.getLong(2);
			Long idPedido = set.getLong(3);
			Integer quantidade = set.getInt(4);
			BigDecimal valorTotal = set.getBigDecimal(5);
			return new Item (id, idProduto, idPedido, quantidade, valorTotal);
		}
		return null;
	}
	
	public void deletarItem(Long id) throws SQLException {
		if(id!=null) {
			PreparedStatement itemDeleteStmt;
			itemDeleteStmt = conn.prepareStatement(" DELETE FROM Item WHERE id = ?; ");
			itemDeleteStmt.setLong(1, id);
			itemDeleteStmt.execute();
		}
	}
	
	public void deletarTodos() throws SQLException {
		PreparedStatement itemDeleteStmt;
		itemDeleteStmt = conn.prepareStatement(" DELETE FROM Item; ");
		itemDeleteStmt.execute();
	}

	public List<Item> getItems() {
		List<Item> items = new ArrayList<>();
		PreparedStatement itemStmt;
		try {
			itemStmt = conn.prepareStatement(" SELECT id, id_produto, id_pedido, quantidade, valor_total FROM Item ORDER BY id DESC; ");
			ResultSet itemResults = itemStmt.executeQuery();
			while (itemResults.next()) {
				items.add(convertIntoObject(itemResults));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return items;
	}
	
	public void inserirItemAleatorio(Long idProduto, Long idPedido) throws SQLException {
		PreparedStatement itemStmt;
		itemStmt = conn.prepareStatement(" INSERT INTO Item (id_produto, id_pedido, quantidade, valor_total) VALUES (?, ?, ?, ?); ");
		itemStmt.setLong(1, idProduto);
		itemStmt.setLong(2, idPedido);
		itemStmt.setInt(3, (int)Math.round(Math.random()*99));
		itemStmt.setBigDecimal(4, getRandomValorTotal());
		itemStmt.execute();
	}
	
	private BigDecimal getRandomValorTotal() {
		String price = "";
		price += Math.round(Math.random()*999);
		price += ".";
		price += Math.round(Math.random()*99);
		return new BigDecimal(price);
	}
	
}