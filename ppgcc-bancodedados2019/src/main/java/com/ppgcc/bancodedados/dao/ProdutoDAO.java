package com.ppgcc.bancodedados.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ppgcc.bancodedados.domain.Produto;

public class ProdutoDAO {

	private Connection conn;

	public ProdutoDAO(Connection conn) {
		super();
		this.conn = conn;
	}
	
	private Produto convertIntoObject(ResultSet set) throws SQLException {
		if (set!=null) {
			Long id = set.getLong(1);
			String descricao = set.getString(2);
			BigDecimal preco = set.getBigDecimal(3);
			return new Produto (id, descricao, preco);
		}
		return null;
	}

	public List<Produto> getProdutos() {
		List<Produto> produtos = new ArrayList<>();
		PreparedStatement produtoStmt;
		try {
			produtoStmt = conn.prepareStatement(" SELECT id, descricao, preco FROM Produto ORDER BY id DESC; ");
			ResultSet produtoResults = produtoStmt.executeQuery();
			while (produtoResults.next()) {
				produtos.add(convertIntoObject(produtoResults));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return produtos;
	}
	
	public void deletarProduto(Long id) throws SQLException {
		if(id!=null) {
			PreparedStatement produtoDeleteStmt;
			produtoDeleteStmt = conn.prepareStatement(" DELETE FROM Produto WHERE id = ?; ");
			produtoDeleteStmt.setLong(1, id);
			produtoDeleteStmt.execute();
		}
	}
	
	public void deletarTodos() throws SQLException {
		PreparedStatement produtoDeleteStmt;
		produtoDeleteStmt = conn.prepareStatement(" DELETE FROM Produto; ");
		produtoDeleteStmt.execute();
	}
	
	public void inserirProdutoAleatorio() throws SQLException {
		PreparedStatement produtoStmt;
		produtoStmt = conn.prepareStatement(" INSERT INTO Produto (descricao, preco) VALUES (?, ?); ");
//		for(int i=0; i<100; i++) System.out.println("Produto: "+new Produto((long)0, getRandomDescricao(), getRandomPreco()));
		produtoStmt.setString(1, getRandomDescricao());
		produtoStmt.setBigDecimal(2, getRandomPreco());
		produtoStmt.execute();
	}
	
	private String getRandomDescricao() {
		List<String> fName = Arrays.asList("radio", "twister", "blanket", "magnet", "sun glasses", "rubber band", "truck", "stockings", "street lights", "soap", "bottle cap", "shampoo", "ring", "nail file", "toothpaste", "sketch pad", "newspaper", "cat", "cup", "cork", "ipod", "washing machine", "outlet", "eraser", "sailboat", "bag", "bow", "knife", "lip gloss", "rusty nail", "plate", "floor", "brocolli", "slipper", "mop", "chocolate", "wagon", "tree", "shoe lace", "door", "picture frame", "nail clippers", "wallet", "tooth picks", "drill press", "video games", "bookmark", "table", "lotion", "desk");
		List<String> lName = Arrays.asList("air freshener", "slipper", "tissue box", "bottle", "wallet", "cup", "playing card", "rusty nail", "boom box", "vase", "car", "greeting card", "mirror", "newspaper", "carrots", "coasters", "drill press", "computer", "cell phone", "tomato", "plastic fork", "USB drive", "charger", "money", "water bottle", "photo album", "CD", "paper", "rubber band", "tree", "doll", "lamp shade", "white out", "window", "scotch tape", "chair", "chalk", "eye liner", "table", "sand paper", "shovel", "thermostat", "cinder block", "lamp", "floor", "truck", "bracelet", "stockings", "toothbrush", "tv");
		int frandom = (int)Math.round(Math.random()*(fName.size()-1));
		int lrandom = (int)Math.round(Math.random()*(lName.size()-1));
		return fName.get(frandom)+" "+lName.get(lrandom);
	}
	
	private BigDecimal getRandomPreco() {
		String price = "";
		price += Math.round(Math.random()*999);
		price += ".";
		price += Math.round(Math.random()*99);
		return new BigDecimal(price);
	}
	
}