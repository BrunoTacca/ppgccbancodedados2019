package com.ppgcc.bancodedados.domain;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

public class Cliente {

	private Long id;
	private String nome;
	private String cpf;
	private String telefone;

	public Cliente(Long id, String nome, String cpf, String telefone) {
		super();
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.telefone = telefone;
	}
	
	public String toString() {
	    MaskFormatter mf;
		try {
			mf = new MaskFormatter("###.###.###-##");
		    mf.setValueContainsLiteralCharacters(false);
			return "["+id+"] "+nome+" | "+mf.valueToString(cpf)+" | "+telefone;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}


}