package com.ppgcc.bancodedados.domain;

import java.math.BigDecimal;

public class Item {

	private Long id;
	private Long idProduto;
	private Long idPedido;
	private Integer quantidade;
	private BigDecimal valorTotal;
	public Item(Long id, Long idProduto, Long idPedido, Integer quantidade, BigDecimal valorTotal) {
		super();
		this.id = id;
		this.idProduto = idProduto;
		this.idPedido = idPedido;
		this.quantidade = quantidade;
		this.valorTotal = valorTotal;
	}
	public String toString() {
		return "["+id+"] [Prod:"+idProduto+"] [Ped:"+idPedido+"] | "+quantidade+" x R$ "+valorTotal;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdProduto() {
		return idProduto;
	}
	public void setIdProduto(Long idProduto) {
		this.idProduto = idProduto;
	}
	public Long getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(Long idPedido) {
		this.idPedido = idPedido;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public BigDecimal getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}
	

}