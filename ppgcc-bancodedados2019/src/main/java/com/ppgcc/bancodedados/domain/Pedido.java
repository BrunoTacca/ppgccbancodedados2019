package com.ppgcc.bancodedados.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Pedido {

	private Long id;
	private Long idCliente;
	private LocalDateTime dataHora;
	private BigDecimal valorTotal;
	public Pedido(Long id, Long idCliente, LocalDateTime dataHora, BigDecimal valorTotal) {
		super();
		this.id = id;
		this.idCliente = idCliente;
		this.dataHora = dataHora;
		this.valorTotal = valorTotal;
	}
	public String toString() {
		return "["+id+"] [C:"+idCliente+"] | R$ "+valorTotal+" | "+dataHora.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public LocalDateTime getDataHora() {
		return dataHora;
	}
	public void setDataHora(LocalDateTime dataHora) {
		this.dataHora = dataHora;
	}
	public BigDecimal getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	
	


}