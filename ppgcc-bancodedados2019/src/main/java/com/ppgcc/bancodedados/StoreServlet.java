package com.ppgcc.bancodedados;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.ppgcc.bancodedados.dao.ClienteDAO;
import com.ppgcc.bancodedados.dao.ItemDAO;
import com.ppgcc.bancodedados.dao.PedidoDAO;
import com.ppgcc.bancodedados.dao.ProdutoDAO;
import com.ppgcc.bancodedados.domain.Cliente;
import com.ppgcc.bancodedados.domain.Item;
import com.ppgcc.bancodedados.domain.Pedido;
import com.ppgcc.bancodedados.domain.Produto;

@WebServlet(name = "Store", urlPatterns = {"/store"})
public class StoreServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(StoreServlet.class.getName());

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		DataSource pool = (DataSource) req.getServletContext().getAttribute("ppgcc-pool");
		List<Cliente> clientes = new ArrayList<Cliente>();
		List<Produto> produtos = new ArrayList<Produto>();
		List<Item> itens = new ArrayList<Item>();
		List<Pedido> pedidos = new ArrayList<Pedido>();
		try (Connection conn = pool.getConnection()) {
			ClienteDAO clienteDAO = new ClienteDAO(conn);
			clientes = clienteDAO.getClientes();

			ProdutoDAO produtoDAO = new ProdutoDAO(conn);
			produtos = produtoDAO.getProdutos();

			ItemDAO itemDAO = new ItemDAO(conn);
			itens = itemDAO.getItems();

			PedidoDAO pedidoDAO = new PedidoDAO(conn);
			pedidos = pedidoDAO.getPedidos();

		} catch (SQLException ex) {
			throw new ServletException("Não foi possível conectar ao banco de dados.", ex);
		}

		req.setAttribute("clientes", clientes);
		req.setAttribute("produtos", produtos);
		req.setAttribute("itens", itens);
		req.setAttribute("pedidos", pedidos);
		req.getRequestDispatcher("/store.jsp").forward(req, resp);

	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		String action = req.getParameter("a");
		
		if(action==null) {
			resp.setStatus(400);
			resp.getWriter().append("Ação invalida.");
			return;
		}
		action = action.toUpperCase();
		
		String table = req.getParameter("table");
		if (table != null) {
			table = table.toUpperCase();
		}
		
		if (table == null || (!table.equals("CLIENTE") && !table.equals("PRODUTO") && !table.equals("ITEM") && !table.equals("PEDIDO"))) {
			resp.setStatus(400);
			resp.getWriter().append("Tabela invalida.");
			return;
		}
		
		if(action.equals("ADD")) {
			add(req,resp,table);
		} else
		if(action.equals("DEL")) {
			del(req,resp,table);
		}
	}
	
	private void add(HttpServletRequest req, HttpServletResponse resp, String table) throws IOException {

		DataSource pool = (DataSource) req.getServletContext().getAttribute("ppgcc-pool");
		try (Connection conn = pool.getConnection()) {

			if(table.equals("CLIENTE")) {
				ClienteDAO clienteDAO = new ClienteDAO(conn);
				clienteDAO.inserirClienteAleatorio();
			} else
			if(table.equals("PRODUTO")) {
				ProdutoDAO produtoDAO = new ProdutoDAO(conn);
				produtoDAO.inserirProdutoAleatorio();
			} else
			if(table.equals("ITEM")) {
				ItemDAO itemDAO = new ItemDAO(conn);
				ProdutoDAO produtoDAO = new ProdutoDAO(conn);
				PedidoDAO pedidoDAO = new PedidoDAO(conn);
				List<Produto> produtos = new ArrayList<Produto>();
				List<Pedido> pedidos = new ArrayList<Pedido>();
				produtos = produtoDAO.getProdutos();
				pedidos = pedidoDAO.getPedidos();

				itemDAO.inserirItemAleatorio(produtos.get((int)Math.round(Math.random()*(produtos.size()-1))).getId(),
											 pedidos.get((int)Math.round(Math.random()*(pedidos.size()-1))).getId());
			} else
			if(table.equals("PEDIDO")) {
				PedidoDAO pedidoDAO = new PedidoDAO(conn);
				ClienteDAO clienteDAO = new ClienteDAO(conn);
				List<Cliente> clientes = new ArrayList<Cliente>();
				clientes = clienteDAO.getClientes();
				pedidoDAO.inserirPedidoAleatorio(clientes.get((int)Math.round(Math.random()*(clientes.size()-1))).getId());
			}

		} catch (SQLException ex) {
			LOGGER.log(Level.WARNING, "Falha ao inserir na tabela "+table+".", ex);
			resp.setStatus(500);
			resp.getWriter().write("Falha ao inserir na tabela "+table+"! Verifique o log.");
		}
		
	    resp.setStatus(200);
	    resp.getWriter().printf(" Elemento inserido com sucesso em '%s'!\n", table);
	}
	
	private void del(HttpServletRequest req, HttpServletResponse resp, String table) throws IOException {
		Long id = Long.parseLong(req.getParameter("id"));
		
		DataSource pool = (DataSource) req.getServletContext().getAttribute("ppgcc-pool");
		try (Connection conn = pool.getConnection()) {

			if(table.equals("CLIENTE")) {
				ClienteDAO clienteDAO = new ClienteDAO(conn);
				if(id!=null && id>0) clienteDAO.deletarCliente(id);
				else clienteDAO.deletarTodos();
			} else
			if(table.equals("PRODUTO")) {
				ProdutoDAO produtoDAO = new ProdutoDAO(conn);
				if(id!=null && id>0) produtoDAO.deletarProduto(id);
				else produtoDAO.deletarTodos();
			} else
			if(table.equals("ITEM")) {
				ItemDAO itemDAO = new ItemDAO(conn);
				if(id!=null && id>0) itemDAO.deletarItem(id);
				else itemDAO.deletarTodos();
			} else
			if(table.equals("PEDIDO")) {
				PedidoDAO pedidoDAO = new PedidoDAO(conn);
				if(id!=null && id>0) pedidoDAO.deletarPedido(id);
				else pedidoDAO.deletarTodos();
			}

		} catch (SQLException ex) {
			LOGGER.log(Level.WARNING, "Falha ao inserir na tabela id: "+id+".", ex);
			resp.setStatus(500);
			resp.getWriter().write("Falha ao inserir na tabela, id: "+id+"! Verifique o log.");
		}
		
	    resp.setStatus(200);
		if(id!=null && id>0) resp.getWriter().printf(" Elemento inserido com sucesso em '%s'!\n", id);
		else resp.getWriter().printf(" Tabela limpa com sucesso!\n");
	    
	}

}