<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <title>Tabs VS Spaces</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
	<style>
		.collection-item.avatar {
			min-height: unset !important;
		}
	</style>
</head>
<body>
<nav class="red lighten-1">
    <div class="nav-wrapper">
        <a href="#" class="brand-logo center">PPGCC - Demo SQL</a>
    </div>
</nav>
<div class="section">
    <div class="row center">
        <div class="col s3">
            <div class="card-panel green lighten-3">
                <i class="material-icons face black"></i>
                <button id="addCliente" class="btn green">ADD Cliente</button> <button class="btn red" onclick="del(-1,'cliente');">CLEAN</button>
            </div>
        </div>
        <div class="col s3">
            <div class="card-panel green lighten-3">
                <i class="material-icons face black"></i>
                <button id="addProduto" class="btn green">ADD Produto</button> <button class="btn red" onclick="del(-1,'produto');">CLEAN</button>
            </div>
        </div>
        <div class="col s3">
            <div class="card-panel green lighten-3">
                <i class="material-icons face black"></i>
                <button id="addItem" class="btn green">ADD Item</button> <button class="btn red" onclick="del(-1,'item');">CLEAN</button>
            </div>
        </div>
        <div class="col s3">
            <div class="card-panel green lighten-3">
                <i class="material-icons face black"></i>
                <button id="addPedido" class="btn green">ADD Pedido</button> <button class="btn red" onclick="del(-1,'pedido');">CLEAN</button>
            </div>
        </div>
    </div>
    <h4 class="header center">Clientes</h4>
    <ul class="container collection center">
        <c:forEach items="${clientes}" var="cliente">
            <li class="collection-item avatar">
                <p><c:out value="${cliente}"/> <button class="btn red" onclick="del(${cliente.id},'cliente');">DEL</button></p>
            </li>
        </c:forEach>
    </ul>
    <h4 class="header center">Produtos</h4>
    <ul class="container collection center">
        <c:forEach items="${produtos}" var="produto">
            <li class="collection-item avatar">
                <p><c:out value="${produto}"/> <button class="btn red" onclick="del(${produto.id},'produto');">DEL</button></p>
            </li>
        </c:forEach>
    </ul>
    <h4 class="header center">Itens</h4>
    <ul class="container collection center">
        <c:forEach items="${itens}" var="item">
            <li class="collection-item avatar">
                <p><c:out value="${item}"/> <button class="btn red" onclick="del(${item.id},'item');">DEL</button></p>
            </li>
        </c:forEach>
    </ul>
    <h4 class="header center">Pedidos</h4>
    <ul class="container collection center">
        <c:forEach items="${pedidos}" var="pedido">
            <li class="collection-item avatar">
                <p><c:out value="${pedido}"/> <button class="btn red" onclick="del(${pedido.id},'pedido');">DEL</button></p>
            </li>
        </c:forEach>
    </ul>
</div>
<script>
	function del(id, table) {
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			if (this.readyState == 4) {
				if (!window.alert(this.responseText)) {
					window.location.reload();
				}
			}
		};
		xhr.open("POST", "/store", true);
		xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xhr.send("a=del&table="+table+"&id=" + id);
	}
	
	function add(table) {
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			if (this.readyState == 4) {
				if (!window.alert(this.responseText)) {
					window.location.reload();
				}
			}
		};
		xhr.open("POST", "/store", true);
		xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xhr.send("a=add&table=" + table);
	}

	document.getElementById("addCliente").addEventListener("click",function() {add("Cliente");});
	document.getElementById("addProduto").addEventListener("click",function() {add("Produto");});
	document.getElementById("addItem").addEventListener("click",function() {add("Item");});
	document.getElementById("addPedido").addEventListener("click",function() {add("Pedido");});
	
</script>
</body>
</html>